const express = require("express");
const productController = require("../controllers/productController");

const router = express.Router();

router
  .route("/")
  .get(productController.getAllProducts)
  .post(productController.createProducts);

router
  .route("/:id")
  .get(productController.getOneProducts)
  .patch(productController.updateProducts)
  .delete(productController.deleteProducts);

module.exports = router;
