const express = require("express");
const categoryController = require("../controllers/categoryController");

const router = express.Router();

router
  .route("/")
  .get(categoryController.getAllCategories)
  .post(categoryController.createCategories);

router
  .route("/:id")
  .get(categoryController.getOneCategories)
  .patch(categoryController.updateCategories)
  .delete(categoryController.deleteCategories);

module.exports = router;
