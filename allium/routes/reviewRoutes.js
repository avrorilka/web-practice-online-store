const express = require("express");
const reviewsController = require("../controllers/reviewsController");

const router = express.Router();

router
  .route("/")
  .get(reviewsController.getAllReviews)
  .post(reviewsController.createReviews);

router
  .route("/:id")
  .get(reviewsController.getOneReviews)
  .patch(reviewsController.updateReviews)
  .delete(reviewsController.deleteReviews);

module.exports = router;
