const sequelize = require("../db.js");
const Category = sequelize.models.category;
const factory = require("./handlerFactory");

exports.getAllCategories = factory.getAll(Category);
exports.getOneCategories = factory.getOne(Category);
exports.createCategories = factory.createOne(Category);
exports.updateCategories = factory.updateOne(Category);
exports.deleteCategories = factory.deleteOne(Category);
