const sequelize = require("../db.js");
const Review = sequelize.models.review;
const factory = require("./handlerFactory");

exports.getAllReviews = factory.getAll(Review);
exports.getOneReviews = factory.getOne(Review);
exports.createReviews = factory.createOne(Review);
exports.updateReviews = factory.updateOne(Review);
exports.deleteReviews = factory.deleteOne(Review);
