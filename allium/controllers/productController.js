const multer = require("multer");
const sharp = require("sharp");
const sequelize = require("../db.js");
const Product = sequelize.models.product;
const Category = sequelize.models.category;
const Product = sequelize.models.product;
const Price = sequelize.models.price;
const CatProduct = sequelize.models.category_product;
const catchAsync = require("../utils/catchAsync");
const AppError = require("../utils/appError");
const factory = require("./handlerFactory");

const multerStorage = multer.memoryStorage();

const multerFilter = (req, file, cb) => {
  if (file.mimetype.startsWith("image")) {
    cb(null, true);
  } else {
    cb(new AppError("Not an image! Please upload only images.", 400), false);
  }
};

const upload = multer({
  storage: multerStorage,
  fileFilter: multerFilter,
});

exports.uploadUserPhoto = upload.single("photo");

exports.resizeUserPhoto = catchAsync(async (req, res, next) => {
  if (!req.file) return next();

  req.file.filename = `user-${req.product.id}-${Date.now()}.jpeg`;

  await sharp(req.file.buffer)
    .resize(500, 500)
    .toFormat("jpeg")
    .jpeg({ quality: 90 })
    .toFile(`public/img/users/${req.file.filename}`);

  next();
});

exports.getAllProducts = factory.getAll(Product);
exports.getOneProducts = factory.getOne(Product);
exports.createProducts = factory.createOne(Product);
exports.updateProducts = factory.updateOne(Product);
exports.deleteProducts = factory.deleteOne(Product);

exports.getOverview = catchAsync(async (req, res, next) => {
  let products = await Product.findAll({
    include: [
      {
        model: Price,
        as: "id_price_price",
      },
    ],
    limit: 4,
    order: [["createdAt", "DESC"]],
  });

  res.status(200).json({
    status: "success",
    results: products.length,
    data: {
      data: products,
    },
  });
});

exports.getTotalAmount = catchAsync(async (req, res, next) => {
  sequelize.query("CALL GetProductsAmount();").then((result) => {
    res.status(200).json({
      status: "success",
      results: result.length,
      data: {
        data: result,
      },
    });
  });
});

exports.getOrderAmount = catchAsync(async (req, res, next) => {
  sequelize.query("CALL GetOrderProductAmount();").then((result) => {
    res.status(200).json({
      status: "success",
      results: result.length,
      data: {
        data: result,
      },
    });
  });
});

exports.getCatalog = catchAsync(async (req, res, next) => {
  Product.belongsToMany(Category, {
    through: CatProduct,
    foreignKey: "id_product",
  });
  Category.belongsToMany(Product, {
    through: CatProduct,
    foreignKey: "id_category",
  });

  let products;

  if (!req.query.cat) {
    products = await Product.findAll({
      include: [
        {
          model: Price,
          as: "id_price_price",
        },
      ],
    });
  } else {
    products = await Product.findAll({
      include: [
        {
          model: Category,
          where: {
            id: req.query.cat,
          },
        },
        {
          model: Price,
          as: "id_price_price",
        },
      ],
    });
  }

  const categories = await Category.findAll();
  res.status(200).json({
    status: "success",
    data: {
      products,
      categories,
    },
  });
});

exports.getProduct = catchAsync(async (req, res, next) => {
  const product = await Product.findOne({
    where: {
      id: req.params.id,
    },
    include: [
      {
        model: Price,
        as: "id_price_price",
      },
    ],
  });

  if (!product) {
    return next(new AppError("There is no product with that name.", 404));
  }

  res.status(200).json({
    status: "success",
    results: product.length,
    data: {
      data: product,
    },
  });
});
