const sequelize = require("../db.js");
const User = sequelize.models.user;
const UserGroup = sequelize.models.user_group;
const factory = require("./handlerFactory");

let objUserGroup = {
  model: UserGroup,
  as: "user_groups",
  foreignKey: "id_group",
  attributes: ["id_group"],
};

exports.getAllUsers = factory.getAll(User, [objUserGroup]);
exports.getOneUser = factory.getOne(User, [objUserGroup]);
exports.createUser = factory.createOne(User);
exports.updateUser = factory.updateOne(User);
exports.deleteUser = factory.deleteOne(User);

exports.getMe = (req, res, next) => {
  req.params.id = req.user.id;
  next();
};
