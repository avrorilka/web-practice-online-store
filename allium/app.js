const path = require("path");
const AppError = require("./utils/appError");
const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");

const adminBroRouter = require("./routes/authRoutes.js");
const viewRouter = require("./routes/viewRoutes");
const userRouters = require("./routes/userRoutes");
const categoryRouter = require("./routes/categoryRouter");
const productRoutes = require("./routes/productRoutes");
const reviewRoutes = require("./routes/reviewRoutes");

const express = require("express");
const app = express();

app.set("view engine", "pug");
app.set("views", path.join(__dirname, "views"));
app.use(express.static(path.join(__dirname, "public")));

app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use("/", viewRouter);
app.use("/admin", adminBroRouter);
app.use("/api/users", userRouters);
app.use("/api/products", productRoutes);
app.use("/api/categories", categoryRouter);
app.use("/api/reviews", reviewRoutes);

app.all("*", (req, res, next) => {
  next(
    new AppError(`Не можу знайти ${req.originalUrl} на цьому сервері!`, 404)
  );
});

app.listen(3000, () => console.log("Running on localhost:3000"));
